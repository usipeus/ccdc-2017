#!/usr/bin/python

import hashlib, os, sys, ConfigParser

# config location
conf_path = "/var/log/web-integrity.ini"

# enumerate all relevant files

# CentOS, Debian
path = "./var/www/"

# Win 2012
# path = ""

file_list = []
for root, dirs, fns in os.walk(path):
    for fn in fns:
        file_list.append(os.path.join(root, fn))

# open config
conf = ConfigParser.ConfigParser()
conf.read(conf_path)

# open log file
log_path = "/var/log/web-integrity.log"
try:
    lagf = open(fname, "a")
except IOError:
    print "DEBUG: could not open logfile", log_path

# hash all web pages with sha-256 and sha-512
for fname in file_list:
    # open file
    try:
        f = open(fname, "r")
    except IOError:
        print "DEBUG: could not open", fname
        continue
    contents = f.read()
    # sha256
    sha256digest = hashlib.sha256(contents)
    # sha512
    sha512digest = hashlib.sha512(contents)

    # save as a Config if not seen before
    if fname not in conf.sections():
        conf.add_section(fname)
        conf.set(fname, "sha256", sha256digest.hexdigest())
        conf.set(fname, "sha512", sha512digest.hexdigest())
    else:
        # else, compare hashes
        conf_sha256 = conf.get(fname, "sha256")
        conf_sha512 = conf.get(fname, "sha512")
        if sha256digest.hexdigest() != conf_sha256 or \
            sha512digest.hexdigest() != conf_sha512:
            # throw alert
            lagf.write("WEB INTEGRITY VIOLATED: " + fname + "\n")

try:
    cfg = open(conf_path, "wb")
except IOError:
    print "ERROR: could not write conf file"
    sys.exit(1)

conf.write(cfg)

